all: deploy

deploy: alsatest
	scp alsatest root@192.168.7.2:.

alsatest: alsatest.cpp common_alsa.cpp common_alsa.h
	${CXX} -Wno-write-strings alsatest.cpp common_alsa.cpp -lasound -lpthread -o alsatest

clean:
	rm -f alsatest
