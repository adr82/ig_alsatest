#define ALSA_PCM_NEW_HW_PARAMS_API
#define ALSA_PCM_NEW_SW_PARAMS_API
#include <alsa/asoundlib.h>

#define FORMAT SND_PCM_FORMAT_S16_LE

// very quick way to check I2C register values
void i2cget() {
    char reg1[64] = { 0 };
    char reg2[64] = { 0 };

    FILE* fp = popen("i2cget -f -y 0 0x1a 0x91", "r");
    if(fp != NULL) {
        fgets(reg1, sizeof(reg1), fp);
        reg1[strcspn(reg1, "\n")] = 0;
        pclose(fp);
    }
    fp = popen("i2cget -f -y 0 0x1a 0x95", "r");
    if(fp != NULL) {
        fgets(reg2, sizeof(reg2), fp);
        reg2[strcspn(reg2, "\n")] = 0;
        pclose(fp);
    }
    printf("Reg 0x91 = %s | Reg 0x95 = %s\n", reg1, reg2);
}

// basic method for setting a register
void i2cset() {
    system("i2cset -f -y 0 0x1a 0x91 0x83");
}

/*
    Check if err<0; if so, there is an error.
    Print the given error message, and the string representation of the message
    and then exit with an error code.
*/
void alsa_check_error(int err, char *msg)
{
    if(err<0)
    {
        fprintf(stderr, "ALSA:%s error code:%s\n", msg, snd_strerror(err));
        exit(1);
    }
}

/* Close the ALSA device */
void common_alsa_close(snd_pcm_t* pcm_handle)
{   
    int err;

    err = snd_pcm_drop(pcm_handle);
    alsa_check_error(err, "Error stopping stream");

    err = snd_pcm_close(pcm_handle);
    alsa_check_error(err, "Error closing stream");

}

int common_alsa_xrun_recovery(snd_pcm_t *handle, int err) 
{
    if(err == -EPIPE) 
    {
        err = snd_pcm_prepare(handle);
        if(err < 0)
            fprintf(stderr, "Can't recover from underrun: %s\n", snd_strerror(err));
        return 0;
    } 
    else if(err == -ESTRPIPE) 
    {
        while((err = snd_pcm_resume(handle)) == -EAGAIN) 
        {
            usleep(1);
        }
        if(err < 0) 
        {
            err = snd_pcm_prepare(handle);
            if(err < 0)
                fprintf(stderr, "Can't recover from suspend: %s\n", snd_strerror(err));
        }
        return 0;
    }
    return err;
}

/* 
    Set the software parameters for the ALSA device.
    Only needed for playback, not capture.
*/
void alsa_set_sw_params(snd_pcm_t* pcm_handle, int period)
{
    snd_pcm_sw_params_t *sw_params;
    int err;
    
	err = snd_pcm_sw_params_malloc (&sw_params);
    alsa_check_error(err, "Cannot allocate software parameters structure");
    	
	err = snd_pcm_sw_params_current (pcm_handle, sw_params);
    alsa_check_error(err, "Cannot initialise software parameters structure");
    	
    // this sets the minimum number of frames of PCM data that must be available for the 
    // ALSA device to report itself as available
	err = snd_pcm_sw_params_set_avail_min (pcm_handle, sw_params, period); 
    alsa_check_error(err,"Cannot set minimum available count");
		
    // this sets a threshold for the number of frames that must be available before
    // playback is started
	err = snd_pcm_sw_params_set_start_threshold (pcm_handle, sw_params, 2*period); // 0);
    alsa_check_error(err,"Cannot set start mode");

	// Set sw Params
	err = snd_pcm_sw_params (pcm_handle, sw_params);
    alsa_check_error(err,"Cannot set software parameters");

    {
        unsigned long val;
        err = snd_pcm_sw_params_current(pcm_handle, sw_params);
        alsa_check_error(err, "Cannot get software parameters");

        err = snd_pcm_sw_params_get_boundary(sw_params, &val);
        alsa_check_error(err, "Cannot get boundary");

        err = snd_pcm_sw_params_set_stop_threshold(pcm_handle, sw_params, val);
        alsa_check_error(err, "Cannot set stop threshold");

        err = snd_pcm_sw_params_set_silence_threshold(pcm_handle, sw_params, 0);
        alsa_check_error(err, "Cannot set silence threshold");

        err = snd_pcm_sw_params_set_silence_size(pcm_handle, sw_params, val);
        alsa_check_error(err, "Cannot set silence size");
        
    	// Set sw Params
	    err = snd_pcm_sw_params (pcm_handle, sw_params);
        alsa_check_error(err,"Cannot set software parameters");
    }

    snd_pcm_sw_params_free(sw_params);
}

/* 
    Initialise the ALSA device.
    PCM device is pcm_handle     
*/
snd_pcm_t* common_alsa_init(snd_pcm_t* pcm_handle, int is_master, int period, int channels, unsigned sample_rate, snd_pcm_uframes_t* buffer_size, snd_pcm_uframes_t* period_size, unsigned char override_i2c)
{
	snd_pcm_hw_params_t *hw_params;
    int err, mode;
    snd_pcm_stream_t type;
    snd_pcm_access_t access_type;

    fprintf(stderr, "common_alsa_init(is_master=%d, period=%d, channels=%d, sample_rate=%u)\n", is_master, period, channels, sample_rate);
    
    // master = capture, slave = playback
    type = is_master ? SND_PCM_STREAM_CAPTURE : SND_PCM_STREAM_PLAYBACK;

    mode = is_master ? 0 : SND_PCM_NONBLOCK;

    access_type = SND_PCM_ACCESS_RW_INTERLEAVED;

    fprintf(stderr, "[DEBUG] beginning ALSA setup: ");
    i2cset();
    i2cget();

    err = snd_pcm_open (&pcm_handle, "hw:0,0", type, mode); 
    alsa_check_error(err, "Cannot open device");    

	// Allocate hardware parameters
	err = snd_pcm_hw_params_malloc (&hw_params);
    alsa_check_error(err, "Cannot allocate hardware parameter structure");
    
    err = snd_pcm_hw_params_any (pcm_handle, hw_params);
    alsa_check_error(err, "Cannot initialize hardware parameter structure");

    // Set access type
    err = snd_pcm_hw_params_set_access (pcm_handle, hw_params, access_type);
    alsa_check_error(err,"Cannot set access type");

	// Restrict configuration space to contain only real hardware rates
	err = snd_pcm_hw_params_set_rate_resample(pcm_handle, hw_params, 0);
    alsa_check_error(err,"Re-sampling setup failed for play back");
    
	// Set sample format (NOTE: needs changed if buffer type changed!)
	err = snd_pcm_hw_params_set_format(pcm_handle, hw_params, FORMAT);
    alsa_check_error(err,"Cannot set sample format");
    
	// Set sample rate
	err = snd_pcm_hw_params_set_rate_near (pcm_handle, hw_params, &(sample_rate), 0);
    alsa_check_error(err,"Cannot set sample rate");    

	// Set channel count
	err = snd_pcm_hw_params_set_channels (pcm_handle, hw_params, channels);
    alsa_check_error(err, "Cannot set channel count\n");
    
    fprintf(stderr, "[DEBUG] set sample type and sampling rate: ");
    i2cget();

    // Setup by configuring buffer_time & period_time
    // Buffer_size is set automatically as result of buffer_time setting
    unsigned int period_time, buffer_time; 	// period time in us       
    period_time = (unsigned int)(1e6 * (period/(1.0 * sample_rate)));        
    if(is_master)
        buffer_time = 16 * period_time;
    else
        buffer_time = 2 * period_time; 
    /* fprintf(stderr, "Configuring buffer and period times as %uus and %uus...\n", period_time, buffer_time); */

    int dir = 0;
    
    // Set the period time
    dir = 0;
    err = snd_pcm_hw_params_set_period_time_near(pcm_handle, hw_params, &period_time, &dir);
    alsa_check_error(err, "Cannot set period time");        

    // Set the buffer time
    err = snd_pcm_hw_params_set_buffer_time_near (pcm_handle, hw_params, &buffer_time, &dir );
    alsa_check_error(err, "Cannot set buffer time");
    
    /* fprintf(stderr, "Period time is %uus, buffer time is %uus\n", period_time, buffer_time); */
    fprintf(stderr, "[DEBUG] Calling snd_pcm_hw_params: ");
    i2cget();

	// Set HW parameters
	err = snd_pcm_hw_params (pcm_handle, hw_params);
    alsa_check_error(err,"Cannot set parameters");

    fprintf(stderr, "[DEBUG] After calling snd_pcm_hw_params: ");
    i2cget();
    if(override_i2c) {
        i2cset();
        fprintf(stderr, "Setting register 0x91 to 0x83\n");
        i2cget();
    }

    // free the HW parameter structure
    snd_pcm_hw_params_free (hw_params);

    // Set software parameters for the slave mode only
    if(!is_master)
         alsa_set_sw_params(pcm_handle, period);

    fprintf(stderr, "[DEBUG] After applying SW params: ");
    i2cget();
    
    // Prepare PCM device    
	err = snd_pcm_prepare (pcm_handle);
    alsa_check_error(err, "Cannot prepare interface for use");	

    err = snd_pcm_reset(pcm_handle);
    alsa_check_error(err, "Cannot reset PCM");

    // store the true buffer/period size
    err = snd_pcm_get_params(pcm_handle, buffer_size, period_size); 	    
    alsa_check_error(err, "Cannot get params");

    snd_config_update_free_global();

    fprintf(stderr, "[DEBUG] End of ALSA init process: ");
    i2cget();

    return pcm_handle;
}
