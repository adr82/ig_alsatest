#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <pthread.h>
#include <sys/mman.h>
#include <getopt.h>
#include <math.h>

#include "common_alsa.h"

typedef struct {
    unsigned number_runs;
    unsigned run_length_s;
    unsigned buffer_size_samples;
    unsigned char use_realtime;
    unsigned char use_thread;
    unsigned delay_between_runs_s;
    unsigned char use_sine;
    unsigned char override_i2c;
    unsigned char repeat_alsa_init;
    unsigned char use_wav;
} options_t;

options_t options = {
    4, 15, 128, 1, 1, 1, 0, 0, 0, 0
};

// default audio params are 48kHz, 16-bit little-endian samples
// (we have tried other sample rates and sample formats too)
#define SAMPLE_RATE 48000
snd_pcm_format_t FORMAT = SND_PCM_FORMAT_S16_LE;
snd_pcm_t* pcm_handle = NULL;
snd_pcm_uframes_t alsa_buffer_size, alsa_period_size;
int running = 1;
int current_run = 0;

void signal_handler(int signal) {
    // abort current + future runs when this happens
    if(signal == SIGINT)
        running = 0;
}

void print_options() {
    printf("alsatest [options]\n");
    printf("\n");
    printf("\t-n\tNumber of runs (default 4)\n");
    printf("\t-l\tLength of each run in seconds (default 15)\n");
    printf("\t-p\tAudio buffer size in samples (defaults to 128)\n");
    printf("\t-r\tSet to 1/0 to enable/disable use of realtime scheduling (default 1)\n");
    printf("\t-t\tSet to 1/0 to enable/disable running audio loop in a thread (default 1)\n");
    printf("\t-d\tNumber of seconds to pause between each run (default 1)\n");
    printf("\t-s\tSet to 1/0 to enable/disable playing a sine tone instead of empty buffers (default 0)\n");
    printf("\t-i\tOverride I2C register content (default off)\n");
    printf("\t-a\tRepeat ALSA init process twice before audio loop (default off)\n");
    printf("\t-w\tPlay audio from a file in working dir called test_alsatest.wav\n");
}

// set a scheduler (SCHED_FIFO or SCHED_RR), a realtime priority, and a process nice value
void realtime_priority(int priority, int schedtype, int niceval)
{
    struct sched_param schedparm;
    memset(&schedparm, 0, sizeof(schedparm));
    schedparm.sched_priority = priority; 
    sched_setscheduler(0, schedtype, &schedparm);
    setpriority(PRIO_PROCESS, 0, niceval);
}

void fill_buffer_sine(short* buffer) {
    static int phase = 0;
    // fill the buffer with a sine tone if enabled
    for(int i=0;i<options.buffer_size_samples;i++) 
        buffer[i] = sin((2*M_PI*2840.0*phase++)/48000.0) * 32767;
}

// a basic audio playback loop
void* audio_func(void* ) {
    int err = 0;
    int first = 1;

    // number of samples we want ALSA to report available to be filled before
    // copying in a new buffer
    int avail_threshold = options.buffer_size_samples;

    unsigned char* playback_buffer = (unsigned char*)malloc(options.buffer_size_samples * 2);
    memset(playback_buffer, 0, options.buffer_size_samples * 2);

    if(options.use_realtime)
        realtime_priority(50, SCHED_FIFO, -10);

    // ALSA PCM device initialization (largely copid from the pcm.c/test.c ALSA examples)
    if(options.repeat_alsa_init && current_run == 0) {
        fprintf(stderr, "[DEBUG] Performing extra ALSA init/close: " );
        i2cget();

        pcm_handle = common_alsa_init(pcm_handle, 0, options.buffer_size_samples, 1, SAMPLE_RATE, &alsa_buffer_size, &alsa_period_size, options.override_i2c);
        common_alsa_close(pcm_handle);

        fprintf(stderr, "[DEBUG] After 1/2 ALSA inits: ");
        i2cget();
    }
    pcm_handle = common_alsa_init(pcm_handle, 0, options.buffer_size_samples, 1, SAMPLE_RATE, &alsa_buffer_size, &alsa_period_size, options.override_i2c);

    unsigned buffers_played = 0;

    struct timespec timer;
    clock_gettime(CLOCK_MONOTONIC, &timer);

    fprintf(stderr, "[DEBUG] Starting main loop: ");
    i2cget();

    FILE* wav = NULL;
    if(options.use_wav) {
        wav = fopen("test_alsatest.wav", "rb");
        fseek(wav, 44, 0); // skip header
    } 

    while(running && buffers_played < (options.run_length_s * (SAMPLE_RATE / (options.buffer_size_samples * 1.0))))
    {
        // ensures that there are a buffer's worth of samples free in the ALSA device
        // buffer before allowing the loop to continue to playing more audio data
        err = snd_pcm_avail(pcm_handle);
        if(err < 0) 
        {
            err = common_alsa_xrun_recovery(pcm_handle, err);
            first = 1;
            if(err < 0)
                continue;
            err = snd_pcm_avail(pcm_handle);
        }

        if(err < avail_threshold)
        {
            if(first)
            {
                first = 0;
                fprintf(stderr, "restarting PCM from err %d\n", err);
                err = snd_pcm_start(pcm_handle);
                if(err < 0) 
                {
                    fprintf(stderr, "start err %s\n", snd_strerror(err));
                    exit(-1);
                }
            } 
            else 
            {
                err = snd_pcm_wait(pcm_handle, -1);
                if(err < 0) 
                {
                    fprintf(stderr, "xrun recover\n");
                    err = common_alsa_xrun_recovery(pcm_handle, err);
                    if(err < 0) 
                    {
                        fprintf(stderr, "start err %s\n", snd_strerror(err));
                        exit(-1);
                    }
                    first = 1;
                }
            }
            continue;
        }

        if(options.use_wav) {
            size_t read_samples = fread(playback_buffer, 2, 128, wav);
            if(read_samples < 128) {
                fseek(wav, 44, 0);
                fread(playback_buffer + (read_samples * 2), 2, 128 - read_samples, wav);
            }
        }

        if(options.use_sine)
            fill_buffer_sine((short*)playback_buffer);

        // actually play a new buffer
        err = snd_pcm_writei(pcm_handle, playback_buffer, alsa_period_size);
        buffers_played++;

        first = 0;
    }

    struct timespec endtimer;
    clock_gettime(CLOCK_MONOTONIC, &endtimer);
    float time_s = (endtimer.tv_sec - timer.tv_sec) + ((endtimer.tv_nsec - timer.tv_nsec) / 1e9);

    // expected result here is that the number of buffers actually played in the elapsed
    // time will be very close to the number of buffers that could theoretically have
    // been played given the audio parameters. small differences are fine, but the problem
    // is we are seeing a ~2.5% difference between expected and actually played buffers
    printf("\tTotal time: %.2fs\n", time_s);
    printf("\tActual buffers played: %u\n", buffers_played);
    float expected_buffers = time_s * (SAMPLE_RATE / (1.0 * options.buffer_size_samples));
    printf("\tExpected buffers played: %f\n", expected_buffers);
    printf("\tRatio: %.3f%%\n", 100 * (buffers_played / expected_buffers));

    fprintf(stderr, "[DEBUG] End of main loop: ");
    i2cget();

    common_alsa_close(pcm_handle);

    fprintf(stderr, "[DEBUG] Closed ALSA device: ");
    i2cget();

    free(playback_buffer);
    if(options.use_wav)
        fclose(wav);
    return NULL;
}

int main(int argc, char** argv) {

    // handle params via getopt
    while(1) {
        int c = getopt(argc, argv, "n:l:p:r:t:d:s:hiaw");
        if(c == -1)
            break;

        switch(c) {
            case 'h':
                print_options();
                exit(0);
            case 'n':
                options.number_runs = atoi(optarg);
                break;
            case 'l':
                options.run_length_s = atoi(optarg);
                break;
            case 'p':
                options.buffer_size_samples = atoi(optarg);
                break;
            case 'r':
                options.use_realtime = atoi(optarg);
                break;
            case 't':
                options.use_thread = atoi(optarg);
                break;
            case 'd':
                options.delay_between_runs_s = atoi(optarg);
                break;
            case 's':
                options.use_sine = atoi(optarg);
                break;
            case 'i':
                options.override_i2c = 1;
                break;
            case 'a':
                options.repeat_alsa_init = 1;
                break;
            case 'w':
                options.use_wav = 1;
                break;
            default:
                break;
        }
    }
    printf("Runs=%u, length=%u, bufsz=%u, realtime=%s, thread=%s, delay=%u, sine tone=%s, override_i2c=%s, repeat_alsa_init=%s, use_wav=%s\n", 
            options.number_runs, options.run_length_s, options.buffer_size_samples, 
            options.use_realtime ? "yes" : "no", options.use_thread ? "yes" : "no",
            options.delay_between_runs_s, options.use_sine ? "yes" : "no", options.override_i2c ? "yes" : "no",
            options.repeat_alsa_init ? "yes" : "no", options.use_wav ? "yes" : "no");

    signal(SIGINT, signal_handler);

    if(options.use_realtime)
        realtime_priority(50, SCHED_FIFO, -10);
    
    pthread_t audio_thread;
    pthread_attr_t audio_attr;
    pthread_attr_init(&audio_attr);
    pthread_attr_setinheritsched(&audio_attr, PTHREAD_INHERIT_SCHED);

    for(current_run=0;current_run<options.number_runs;current_run++)  {
        if(!running) {
            printf("Aborting due to signal\n");
            break;
        }

        printf("Run %d/%d\n", current_run + 1, options.number_runs);
        printf("================\n");

        if(options.use_thread) {
            printf("Threaded mode\n");
            pthread_create(&audio_thread, &audio_attr, audio_func, NULL);

            pthread_join(audio_thread, NULL);
            pthread_tryjoin_np(audio_thread, NULL);
        } else {
            printf("Non-threaded mode\n");
            audio_func(NULL);
        }
        printf("\n");

        usleep(1e6 * options.delay_between_runs_s);
    }
}
