#ifndef _COMMON_ALSA_H_
#define _COMMON_ALSA_H_

#define ALSA_PCM_NEW_HW_PARAMS_API
#define ALSA_PCM_NEW_SW_PARAMS_API
#include <alsa/asoundlib.h>
#include <stdlib.h>
#include <stdio.h>

snd_pcm_t* common_alsa_init(snd_pcm_t* pcm_handle, int is_master, int period, int channels, unsigned sample_rate, snd_pcm_uframes_t* buffer_size, snd_pcm_uframes_t* period_size, unsigned char override_i2c);
void common_alsa_close(snd_pcm_t* pcm_handle);
int common_alsa_xrun_recovery(snd_pcm_t* pcm_handle, int err);

void i2cget();
void i2cset();

#endif

